/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.person;

/**
 *
 * @author Henry
 */



public class Person {
    // Data fields attributes
    private String name;
    private double height;
    private double weight;

    
    //Constructor
    public Person() {
    }
    //Constructor with specified parameters
    public Person(String name, double height, double weight) {
        this.name = name;
        this.height = height;
        this.weight = weight;
    }
    
    public enum height{CENTIMETERS,INCHES}
    public enum weight{KG, LBS}
    
     public double getBmi(){
        return weight / ( height * height );
    }

    
 
     // getter and setters
     
     public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
