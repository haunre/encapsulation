/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.person;

/**
 *
 * @author Henry
 */
public class TestPerson {
    
    public static void main(String[] args) {
    
   Person obj1 = new Person("Mary", 172.0, 82.0);
   Person obj2 = new Person("Linda",5.10, 152.0);
   
  
   
    System.out.println("The BMI for " + obj1.getName() + " is " + obj1.getBmi());
    
    System.out.println("The BMI for " + obj2.getName() + " is " + obj2.getBmi());
    
    
   
    }
}
